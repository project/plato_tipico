core = 7.x

api = 2

projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = "3.0-rc5"

projects[advanced_help][subdir] = contrib
projects[advanced_help][version] = "1.1"

projects[backup_migrate][subdir] = contrib
projects[backup_migrate][version] = "3.0"

projects[breakpoints][subdir] = contrib
projects[breakpoints][version] = "1.3"

projects[ctools][subdir] = contrib
projects[ctools][version] = "1.5"

projects[coffee][subdir] = contrib
projects[coffee][version] = "2.2"

projects[context][subdir] = contrib
projects[context][version] = "3.5"

projects[context_breakpoint][subdir] = contrib
projects[context_breakpoint][version] = "1.0-beta3"

projects[cs_adaptive_image][subdir] = contrib
projects[cs_adaptive_image][version] = "1.0"

projects[date][subdir] = contrib
projects[date][version] = "2.8"

projects[ds][subdir] = contrib
projects[ds][version] = "2.7"

projects[elysia_cron][subdir] = contrib
projects[elysia_cron][version] = "2.1"

projects[email][subdir] = contrib
projects[email][version] = "1.3"

projects[entity][subdir] = contrib
projects[entity][version] = "1.5"

projects[entity_translation][subdir] = contrib
projects[entity_translation][version] = "1.0-beta3"

projects[fitvids][subdir] = contrib
projects[fitvids][version] = "1.17"

projects[flexslider][subdir] = contrib
projects[flexslider][version] = "2.0-alpha3"

projects[jquery_update][subdir] = contrib
projects[jquery_update][version] = "2.4"

projects[picture][subdir] = contrib
projects[picture][version] = "2.9"

projects[globalredirect][subdir] = contrib
projects[globalredirect][version] = "1.5"

projects[google_analytics][subdir] = contrib
projects[google_analytics][version] = "1.4"

projects[i18n][subdir] = contrib
projects[i18n][version] = "1.4"

projects[i18n][subdir] = contrib
projects[i18n][version] = "1.11"

projects[l10n_client][subdir] = contrib
projects[l10n_client][version] = "1.3"

projects[l10n_update][subdir] = contrib
projects[l10n_update][version] = "1.1"

projects[languageicons][subdir] = contrib
projects[languageicons][version] = "1.1"

projects[libraries][subdir] = contrib
projects[libraries][version] = "2.2"

projects[link][subdir] = contrib
projects[link][version] = "1.3"

projects[masquerade][subdir] = contrib
projects[masquerade][version] = "1.0-rc7"

projects[metatag][subdir] = contrib
projects[metatag][version] = "1.4"

projects[modernizr][subdir] = contrib
projects[modernizr][version] = "3.3"

projects[module_filter][subdir] = contrib
projects[module_filter][version] = "2.0-alpha2"

projects[oa_responsive_regions][subdir] = contrib
projects[oa_responsive_regions][version] = "1.0-beta1"

projects[panels][subdir] = contrib
projects[panels][version] = "3.4"

projects[pathauto][subdir] = contrib
projects[pathauto][version] = "1.2"

projects[pathologic][subdir] = contrib
projects[pathologic][version] = "2.12"

projects[phone][subdir] = contrib
projects[phone][version] = "1.0-beta1"

projects[print][subdir] = contrib
projects[print][version] = "2.0"

projects[rules][subdir] = contrib
projects[rules][version] = "2.7"

projects[resp_img][subdir] = contrib
projects[resp_img][version] = "1.3"

projects[retina_images][subdir] = contrib
projects[retina_images][version] = "1.0-beta4"

projects[security_review][subdir] = contrib
projects[security_review][version] = "1.2"

projects[token][subdir] = contrib
projects[token][version] = "1.5"

projects[transliteration][subdir] = contrib
projects[transliteration][version] = "3.2"

projects[variable][subdir] = contrib
projects[variable][version] = "2.5"

projects[vef_picture][subdir] = contrib
projects[vef_picture][version] = "2.6"

projects[views_slideshow][subdir] = contrib
projects[views_slideshow][version] = "3.1"

projects[views][subdir] = contrib
projects[views][version] = "3.8"

projects[wysiwyg][subdir] = contrib
projects[wysiwyg][version] = "2.2"

projects[xmlsitemap][subdir] = contrib
projects[xmlsitemap][version] = "2.1"

; Themes
projects[rubik][subdir] = contrib
projects[rubik][version] = "4.1"

projects[tamal][subdir] = contrib
projects[tamal][version] = "1.0-rc5"

projects[tao][subdir] = contrib
projects[tao][version] = "3.1"
